#! /usr/bin/env python
#  Copyright (C) 2009  Sebastian Garcia
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Author:
# Sebastian Garcia, sebastian.garcia@agents.fel.cvut.cz, sgarcia@exa.unicen.edu.ar, eldraco@gmail.com
#
# Changelog

# Description
# Program to extract some values from the binetflow file. Not used anymore
#
# TODO


# standard imports
import getopt
import sys
import time
from datetime import datetime
from datetime import timedelta
####################
# Global Variables

debug = 0
vernum = "0.1"
verbose = False
time_window_number = 0
#########


# Print version information and exit
def version():
    print "+----------------------------------------------------------------------+"
    print "| CCDetector.py Version "+ vernum +"                                            |"
    print "| This program is free software; you can redistribute it and/or modify |"
    print "| it under the terms of the GNU General Public License as published by |"
    print "| the Free Software Foundation; either version 2 of the License, or    |"
    print "| (at your option) any later version.                                  |"
    print "|                                                                      |"
    print "| Author: Garcia Sebastian, eldraco@gmail.com                          |"
    print "| UNICEN-ISISTAN, Argentina. CTU, Prague-ATG                           |"
    print "+----------------------------------------------------------------------+"
    print


# Print help information and exit:
def usage():
    print "+----------------------------------------------------------------------+"
    print "| CCDetector.py Version "+ vernum +"                                            |"
    print "| This program is free software; you can redistribute it and/or modify |"
    print "| it under the terms of the GNU General Public License as published by |"
    print "| the Free Software Foundation; either version 2 of the License, or    |"
    print "| (at your option) any later version.                                  |"
    print "|                                                                      |"
    print "| Author: Garcia Sebastian, eldraco@gmail.com                          |"
    print "| UNICEN-ISISTAN, Argentina. CTU, Prague-ATG                           |"
    print "+----------------------------------------------------------------------+"
    print "\nusage: %s <options>" % sys.argv[0]
    print "options:"
    print "  -h, --help                 Show this help message and exit"
    print "  -v, --verbose              Output more information."
    print "  -D, --debug                Debug."
    print "  -f, --file                 Input netflow file to analize. If - is used, netflows are read from stdin. Remember to pass the header!"
    print "  -w, --width                Width of the time window in minutes."
    print "  -o, --output-file          Output file name."
    print
    sys.exit(1)
    


class time_window:
    """
    Class for storing flow data in a time window
    """
    def __init__(self, stateModel):
        global time_window_number
        self.total_flows = 0
        self.starttime = 0
        self.endtime = 0
        self.id = time_window_number
        time_window_number += 1
        self.srcaddrs = []
        self.sports = []
        self.dstaddrs = []
        self.dports = []
        self.dstports = []
        self.byteses = []
        self.total_bytes = 0
        self.durations = []
        self.total_duration = 0
        self.runtimes = []
        self.total_runtime = 0
        self.labels = []
        self.protos = []
        self.states = []
        self.packetses = []
        self.total_packets = 0
        self.stateModel = stateModel

        self.udp_est_ips = []
        self.udp_est_bytes = 0
        self.udp_int_ips = []
        self.udp_est_new_ips = []
        self.udp_est_new_bytes = 0
        self.udp_int_new_ips = []

        self.tcp_CC_est_ips = []
        self.tcp_CC_est_bytes = 0
        self.tcp_CC_est_67_ips = []
        self.TCP_CC_HTTP_Custom_Encryption_bytes = 0
        self.TCP_CC_HTTP_Custom_Encryption_ips = []

        self.tcp_web_ips = []

        self.tcp_google_ips = []

        self.TCP_CC_HTTP_bytes = 0
        self.TCP_CC_HTTP_ips = []
        self.TCP_CC_HTTP_69_bytes = 0
        self.TCP_CC_HTTP_69_ips = []
        self.TCP_CC_HTTP_62_ips = []
        self.TCP_CC_HTTP_70_ips = []
        self.TCP_CC_HTTP_71_ips = []
        self.TCP_CC_HTTP_80_ips = []

        self.tcp_web_bytes = 0
        self.tcp_google_bytes = 0

        self.tcp_ssl_google_ips = []
        self.tcp_ssl_google_bytes = 0

        self.udp_est_dns_ips = []
        self.udp_est_dns_bytes = 0

        self.tcp_custom_enc_ips = []
        self.tcp_custom_enc_bytes = 0


    def set_starttime(self,starttime):
        self.starttime = starttime
        self.endtime = starttime + timedelta(seconds=self.time_window_width)

    def print_data(self):
        """
        Print data
        """
        if verbose:
            print 'Id:{}\tStarttime:{}\tEndtime:{}\t#Flows:{}\t#Packets:{}\t#Srcaddr:{}\t#Dstaddr:{}\t#Bytes:{}\t#labels:{}\t#UDPUniqEst:{}\t#UDPUniqInt:{}\t#UDPTotEstFlow:{}\t#UDPTotIntFlow:{}\t#NewEstUDPCCIP:{}\tTotEstUDPCCIP:{}\t#NewIntUDPCCIP:{}\tTotIntUDPCCIP:{}\tTCPCCFlow:{}\tTCPCCBytes:{}\tWebFlows:{}\tGoogleFlows:{}\tCCHTTPBytes:{}\tCCHTTPFlow:{}\tCCHTTP69Flows:{}\tCCHTTP69Bytes:{}\tTCPCC67FLows\tTCPCC62Flows{}\tTCPCC70Flows{}\tTCPCC71Flows{}\tTCPCC80Flows{}\tUDPCCNewwEstBytes:{}\tWEBBytes:{}\tGoogleBytes:{}\tSSLGoogleFLows:{}\tSSLGoogleBytes:{}\tEstDNSFlows:{}\tEstDNSBytes:{}\tTCPCustEncFlows:{}\tTCPCustEncBytes:{}\tUDPEstBytes:{}'.format(self.id, self.starttime, self.endtime, self.total_flows, self.total_packets, len(self.uniquify(self.srcaddrs)), len(self.uniquify(self.dstaddrs)), self.total_bytes, len(self.uniquify(self.labels)), len(self.uniquify(self.udp_est_ips)), len(self.uniquify(self.udp_int_ips)), len(self.udp_est_ips), len(self.udp_int_ips), len(self.udp_est_new_ips), len(self.stateModel.all_time_est_udp_dstaddrs), len(self.udp_int_new_ips), len(self.stateModel.all_time_int_udp_dstaddrs), len(self.tcp_CC_est_ips), self.tcp_CC_est_bytes , len(self.tcp_web_ips), len(self.tcp_google_ips), self.TCP_CC_HTTP_bytes, len(self.TCP_CC_HTTP_ips), len(self.TCP_CC_HTTP_69_ips), self.TCP_CC_HTTP_69_bytes, len(self.tcp_CC_est_67_ips), len(self.TCP_CC_HTTP_62_ips), len(self.TCP_CC_HTTP_70_ips), len(self.TCP_CC_HTTP_71_ips), len(self.TCP_CC_HTTP_80_ips), self.udp_est_new_bytes, self.tcp_web_bytes, self.tcp_google_bytes, len(self.tcp_ssl_google_ips), self.tcp_ssl_google_bytes, len(self.udp_est_dns_ips), self.udp_est_dns_bytes, len(self.tcp_custom_enc_ips), self.tcp_custom_enc_bytes, self.udp_est_bytes )

        self.output_file.write('{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(self.id, time.mktime(self.starttime.timetuple()) , self.total_flows, self.total_packets, len(self.uniquify(self.srcaddrs)), len(self.uniquify(self.dstaddrs)), self.total_bytes, len(self.uniquify(self.labels)), len(self.uniquify(self.udp_est_ips)), len(self.uniquify(self.udp_int_ips)), len(self.udp_est_ips), len(self.udp_int_ips), len(self.udp_est_new_ips), len(self.stateModel.all_time_est_udp_dstaddrs), len(self.udp_int_new_ips), len(self.stateModel.all_time_int_udp_dstaddrs) , len(self.tcp_CC_est_ips),self.tcp_CC_est_bytes , len(self.tcp_web_ips), len(self.tcp_google_ips), self.TCP_CC_HTTP_bytes, len(self.TCP_CC_HTTP_ips), len(self.TCP_CC_HTTP_69_ips), self.TCP_CC_HTTP_69_bytes, len(self.tcp_CC_est_67_ips), len(self.TCP_CC_HTTP_62_ips), len(self.TCP_CC_HTTP_70_ips), len(self.TCP_CC_HTTP_71_ips), len(self.TCP_CC_HTTP_80_ips), self.udp_est_new_bytes, self.tcp_web_bytes, self.tcp_google_bytes , len(self.tcp_ssl_google_ips), self.tcp_ssl_google_bytes, len(self.udp_est_dns_ips), self.udp_est_dns_bytes, len(self.tcp_custom_enc_ips), self.tcp_custom_enc_bytes , self.udp_est_bytes))


    def add_srcaddr(self,srcaddr):
        self.srcaddr = srcaddr
        self.srcaddrs.append(srcaddr)

    def add_sport(self,sport):
        self.sport = sport
        self.sports.append(sport)

    def add_dstaddr(self,dstaddr):
        self.dstaddr = dstaddr
        self.dstaddrs.append(dstaddr)

    def add_dport(self,dport):
        self.dport = dport
        self.dports.append(dport)

    def add_bytes(self,bytes):
        self.bytes = bytes
        self.byteses.append(bytes)
        self.total_bytes += bytes

    def add_duration(self,duration):
        self.duration = duration
        self.durations.append(duration)
        self.total_duration += duration

    def add_runtime(self,runtime):
        self.runtime = runtime
        self.runtimes.append(runtime)
        self.total_runtime += runtime

    def add_label(self,label):
        self.label = label
        self.labels.append(label)

    def add_proto(self,proto):
        self.proto = proto
        self.protos.append(proto)

    def add_flow_state(self,flow_state):
        self.state = flow_state
        self.states.append(flow_state)

    def add_packets(self,packets):
        self.packets = packets
        self.packetses.append(packets)
        self.total_packets += packets

    def uniquify(self,seq, idfun=None): 
       # order preserving
       if idfun is None:
           def idfun(x): return x
       seen = {}
       result = []
       for item in seq:
           marker = idfun(item)
           # in old Python versions:
           # if seen.has_key(marker)
           # but in new ones:
           if marker in seen: continue
           seen[marker] = 1
           result.append(item)
       return result

    def compute_info(self):
        """
        Compute the info
        """
        try:
            #TCP-Attempt
            #TCP-Established
            #Botnet-V1-UDP-Attempt
            #From-Botnet-V1-DNS
            #UDP-Attempt-DNS
            ########


            # Amount of established UDP flows in the CC channel
            if not ':' in self.srcaddr and 'udp' in self.proto and 'CON' in self.state and self.dport != '53' and self.dport != '137':
                self.udp_est_ips.append(self.dstaddr)
                self.udp_est_bytes += self.bytes
                # Amount of new UDP Establised IPs
                try:
                    is_there = self.stateModel.all_time_est_udp_dstaddrs[self.dstaddr]
                    # Was there, do nothing
                except KeyError:
                    # It is new
                    self.udp_est_new_ips.append(self.dstaddr)
                    # bytes
                    self.udp_est_new_bytes += self.bytes
                    self.stateModel.all_time_est_udp_dstaddrs[self.dstaddr] = ""

            # Amount of attempted UDP flows in the CC channel
            if not ':' in self.srcaddr and 'udp' in self.proto and 'INT' in self.state and self.dport != '53' and self.dport != '137' :
                self.udp_int_ips.append(self.dstaddr)
                # Amount of new UDP attempted IPs
                try:
                    is_there = self.stateModel.all_time_int_udp_dstaddrs[self.dstaddr]
                    # Was there, do nothing
                except KeyError:
                    # It is new
                    self.udp_int_new_ips.append(self.dstaddr)
                    self.stateModel.all_time_int_udp_dstaddrs[self.dstaddr] = ""


            # All TCP-CC-HTTP
            # Amount of bytes of the established TCP flows for the tuple TCP-CC-HTTP-Custom-Encryption
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-CC-HTTP-Custom-Encryption' in self.label:
                # For it self
                self.TCP_CC_HTTP_Custom_Encryption_bytes += self.bytes
                self.TCP_CC_HTTP_Custom_Encryption_ips.append(self.dstaddr)
                # For all the TCP-CC-HTTP together
                self.TCP_CC_HTTP_ips.append(self.dstaddr)
                self.TCP_CC_HTTP_bytes += self.bytes
            # Amount of bytes of the established TCP flows for the tuple TCP-CC-HTTP-69
            elif not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-CC-HTTP-69' in self.label:
                self.TCP_CC_HTTP_69_bytes += self.bytes
                self.TCP_CC_HTTP_69_ips.append(self.dstaddr)
                # For all the TCP-CC-HTTP together
                self.TCP_CC_HTTP_ips.append(self.dstaddr)
                self.TCP_CC_HTTP_bytes += self.bytes

            if not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-CC-HTTP-Custom-Encryption-62' in self.label:
                self.TCP_CC_HTTP_62_ips.append(self.dstaddr)
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-CC-HTTP-Custom-Encryption-70' in self.label:
                self.TCP_CC_HTTP_70_ips.append(self.dstaddr)
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-CC-HTTP-Custom-Encryption-71' in self.label:
                self.TCP_CC_HTTP_71_ips.append(self.dstaddr)
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-CC-HTTP-Custom-Encryption-80' in self.label:
                self.TCP_CC_HTTP_80_ips.append(self.dstaddr)


            # All TCP-CC (all have Custom-Encryption)
            # Amount of established CC TCP flows
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-CC-Custom-Encryption' in self.label:
                self.tcp_CC_est_ips.append(self.dstaddr) 
                self.tcp_CC_est_bytes += self.bytes
            # Amount of established CC TCP flows in IP 84.59.151.27 and port -3285
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-CC-Custom-Encryption-67' in self.label:
                self.tcp_CC_est_67_ips.append(self.dstaddr) 

            # Amount of WEB flows and bytes
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'WEB' in self.label:
                self.tcp_web_ips.append(self.dstaddr) 
                self.tcp_web_bytes += self.bytes
            # Amount of google (and ssl) flows and bytes
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'Google' in self.label:
                self.tcp_google_ips.append(self.dstaddr) 
                self.tcp_google_bytes += self.bytes
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'SSL-Google' in self.label:
                self.tcp_ssl_google_ips.append(self.dstaddr) 
                self.tcp_ssl_google_bytes += self.bytes
            
            # Amount of DNS established flows and bytes
            if not ':' in self.srcaddr and 'udp' in self.proto and 'DNS' in self.label and not 'Attempt' in self.label:
                self.udp_est_dns_ips.append(self.dstaddr) 
                self.udp_est_dns_bytes += self.bytes

            # TCP-Custom-Encryption
            if not ':' in self.srcaddr and 'tcp' in self.proto and 'TCP-Custom-Encryption' in self.label:
                self.tcp_custom_enc_ips.append(self.dstaddr) 
                self.tcp_custom_enc_bytes += self.bytes


        except Exception as inst:
            print 'Problem in compute_info() in class time_window'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)



class stateModels:
    """
    This class handles all the models
    """
    def __init__(self):
        # Hold all the original netflows
        self.original_netflows = {}
        self.current_time_window = False
        self.time_window_width = timedelta(seconds=0)
        # To hold all the dst ip ever seen by the botnet
        self.all_time_est_udp_dstaddrs = {}
        self.all_time_int_udp_dstaddrs = {}

    def add_netflow(self,netflowArray):
        """
        Receives a netflow and adds it to the model
        """
        try:
            global debug
            global verbose

            #if debug:
            #    print '\nAdding netflow: {} '.format(netflowArray)

            # Just in case they are not in the file
            runtime = -1
            sport = -1
            dport = -1

            for col in netflowArray:
                if 'StartTime' in col.keys()[0]:
                    if '/' in str(col.values()[0]):
                        starttime = datetime.strptime(str(col.values()[0]), '%Y/%m/%d %H:%M:%S.%f') 
                    elif '-' in str(col.values()[0]):
                        starttime = datetime.strptime(str(col.values()[0]), '%Y-%m-%d %H:%M:%S.%f') 

                elif 'SrcAddr' in col.keys()[0]:
                    srcaddr = str(col.values()[0])
                elif 'Sport' in col.keys()[0]:
                    sport = str(col.values()[0])
                elif 'DstAddr' in col.keys()[0]:
                    dstaddr = str(col.values()[0])
                elif 'Dport' in col.keys()[0]:
                    dport = str(col.values()[0])
                elif 'TotBytes' in col.keys()[0]:
                    bytes = int(col.values()[0])
                elif 'Dur' in col.keys()[0]:
                    duration = float(col.values()[0])
                elif 'RunTime' in col.keys()[0]:
                    runtime = float(col.values()[0])
                elif 'Label' in col.keys()[0]:
                    label = str(col.values()[0])
                elif 'Proto' in col.keys()[0]:
                    proto = str(col.values()[0]).lower()
                elif 'State' in col.keys()[0]:
                    flow_state = str(col.values()[0])
                elif 'TotPkts' in col.keys()[0]:
                    packets = int(col.values()[0])

            # Fist time window
            if not self.current_time_window:
                self.current_time_window = time_window(self)
                self.current_time_window.time_window_width = self.time_window_width
                self.current_time_window.output_file = self.output_file
                self.current_time_window.set_starttime(starttime)

            elif starttime > self.current_time_window.endtime:
                # Out of the previous time window
                time_diff = starttime-self.current_time_window.endtime
                time_windows_in_the_middle = int(time_diff.seconds/self.time_window_width)
                #print 'Got a starttime: {}, Time diff: {}. twmiddle: {}'.format(starttime, time_diff, time_windows_in_the_middle)
                last_endtime = self.current_time_window.endtime
                while time_windows_in_the_middle:   
                    self.current_time_window.print_data()
                    self.current_time_window = time_window(self)
                    self.current_time_window.time_window_width = self.time_window_width
                    self.current_time_window.output_file = self.output_file
                    self.current_time_window.set_starttime(last_endtime)
                    last_endtime = self.current_time_window.endtime
                    time_windows_in_the_middle = time_windows_in_the_middle - 1

                self.current_time_window.print_data()
                self.current_time_window = time_window(self)
                self.current_time_window.time_window_width = self.time_window_width
                self.current_time_window.output_file = self.output_file
                self.current_time_window.set_starttime(last_endtime)

            self.current_time_window.total_flows += 1
            self.current_time_window.add_srcaddr(srcaddr)
            self.current_time_window.add_sport(sport)
            self.current_time_window.add_dstaddr(dstaddr)
            self.current_time_window.add_dport(dport)
            self.current_time_window.add_bytes(bytes)
            self.current_time_window.add_duration(duration)
            self.current_time_window.add_runtime(runtime)
            self.current_time_window.add_label(label)
            self.current_time_window.add_proto(proto)
            self.current_time_window.add_flow_state(flow_state)
            self.current_time_window.add_packets(packets)
            self.current_time_window.compute_info()

            if debug:
                print 'Stime:{0}, sddr:{1}, sport:{2}, proto:{9}, dddr:{3}, dport:{4}, bytes:{5}, dur:{6}, runtime:{7}, label={8}'.format(starttime, srcaddr, sport, dstaddr, dport, bytes, duration, runtime, label, proto)
            #self.current_time_window.print_data()



        except Exception as inst:
            print 'Problem in add_netflow() in class stateModels'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def process_netflows(self,netflowFile):
        """
        This function takes the netflowFile and parse it. 
        """
        try:
            global debug
            global verbose

            if verbose:
                print 'Processing the netflow file {0}'.format(netflowFile)


            # Create the output file
            self.output_file = open(self.output_file_name,'w')
            self.output_file.write('Id,Starttime,#Flows,#Packets,#Srcaddr,#Dstaddr,#Bytes,#labels,#UDPUniqEst,#UDPUniqInt,#UDPTotEstFlow,#UDPTotIntFlow,#NewEstUDPCCIP,TotEstUDPCCIP,#NewIntUDPCCIP,TotIntUDPCCIP,TCPCCFlow,TCPCCBytes,WebFlows,GoogleFlows,CCHTTPBytes,CCHTTPFlow,CCHTTP69Flows,CCHTTP69Bytes,TCPCC67FLows,TCPCC62Flows,TCPCC70Flows,TCPCC71Flows,TCPCC80Flows,UDPCCNewEstBytes,WEBBytes,GoogleBytes,SSLGoogleFLows,SSLGoogleBytes,EstDNSFlows,EstDNSBytes,TCPCustEncFlows,TCPCustEncBytes,UDPEstBytes\n')
            # Read the netflow and parse the input
            try:
                # From stdin or file?
                if netflowFile == '-':
                    f = sys.stdin
                else:
                    f = open(netflowFile,'r')
            except Exception as inst:
                print 'Some problem opening the input netflow file. In process_netflow()'
                print type(inst)     # the exception instance
                print inst.args      # arguments stored in .args
                print inst           # __str__ allows args to printed directly
                exit(-1)


            # Just to monitor how many lines we read
            self.netflow_id = 0
            line = f.readline().strip()
            self.netflow_id += 1

            ##################
            # Argus processing...

            columnDict = {}
            templateColumnArray = []
            columnArray = []
            columnNames = line.split(',')

            #if debug:
            #    print 'Columns names: {0}'.format(columnNames)

            # So far argus does no have a column Date
            for col in columnNames:
                columnDict[col] = ""
                templateColumnArray.append(columnDict)
                columnDict = {}

            columnArray = templateColumnArray


            # Read the second line to start processing
            line = f.readline().strip()
            self.netflow_id += 1

            # To store the netflows we should put the data in a dict
            self.original_netflows[self.netflow_id] = line

            while (line):
                if debug:
                    print 'Netflow line: {0}'.format(line),

                # Parse the columns
                columnValues = line.split(',')

                #if debug:
                #    print columnValues

                i = 0
                for col in columnValues:
                    tempDict = columnArray[i]
                    tempDictName = tempDict.keys()[0]
                    tempDict[tempDictName] = col
                    columnArray[i] = tempDict
                    i += 1

                #if debug:
                    #print columnArray

                # Add the netflow to the model.
                # The self.netflow_id is the id of this netflow
                self.add_netflow(columnArray)

                
                # Go back to the empty array
                columnArray = templateColumnArray

                line = f.readline().strip()
                self.netflow_id += 1
                self.original_netflows[self.netflow_id] = line

            # End while

            if verbose:
                print 'Amount of lines read: {0}'.format(self.netflow_id)

        except Exception as inst:
            print 'Problem in process_netflow()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)










def main():
    try:
        global debug
        global verbose

        netflowFile = ""
        width = 60
        output_file_name = "output.csv"

        opts, args = getopt.getopt(sys.argv[1:], "vDhf:w:o:", ["help","version","verbose","debug","file=","width=","output-file="])
    except getopt.GetoptError: usage()

    for opt, arg in opts:
        if opt in ("-h", "--help"): usage()
        if opt in ("-v", "--verbose"): verbose = True
        if opt in ("-D", "--debug"): debug = 1
        if opt in ("-f", "--file"): netflowFile = str(arg)
        if opt in ("-w", "--width"): width = float(arg)*60
        if opt in ("-o", "--output-file"): output_file_name = str(arg)
    try:
        try:
            if debug:
                verbose = True

            if netflowFile == "":
                usage()
                sys.exit(1)

            elif netflowFile != "":
                stateModel = stateModels()
                stateModel.time_window_width = width
                stateModel.output_file_name = output_file_name
                stateModel.process_netflows(netflowFile)

            else:
                    usage()
                    sys.exit(1)

        except Exception, e:
                print "misc. exception (runtime error from user callback?):", e
        except KeyboardInterrupt:
                sys.exit(1)


    except KeyboardInterrupt:
        # CTRL-C pretty handling.
        print "Keyboard Interruption!. Exiting."
        sys.exit(1)


if __name__ == '__main__':
    main()

